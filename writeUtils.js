'use strict';
var wu;

wu = {
  counter: 0,
  debug: 0,

  /**
   * c() is a console.log() replacement with a
   * few enhancements, with debugging in mind.
   *
   * If nothing is passed as an argument to the,
   * function, c() logs a line separator.
   *
   * If a list of arguments is passed, c() logs
   * them in the same line.
   *
   * If an array is passed to the function, c()
   * will log them in separate lines.
   *
   * @param <Undefined, String, Array> arguments
   *
   */
  c: function consoleLog() {

    var i;
    var arg;

    if (arguments.length) {
      // if arguments come inside an array at arguments[0], print them in separate lines.
      if (arguments.length === 1 && Array.isArray(arguments[0])) {

        arguments[0].forEach(function (el) {
          console.log(el);

        });

        return wu;
      }
      else {
        arg = arguments[0];
        i = 1;

        while (arguments[i]) {
          arg += ' ' + arguments[i];
          i++;
        }
        console.log(arg);
        return wu;
      }
    }
    else {
      console.log('<================ random line separator ================>');
      return wu;
    }
  },

  /**
   * i() keeps a counter and console.logs a number.
   *
   * @param <Undefined>
   *
   */
  i: function increment() {
    wu.counter++;
    console.log(wu.counter);
    return wu;
  },

  /**
   * d() keeps a counter and console.logs a debug
   * 'screen' for easy identification, using the
   * c() function to display the desired text.
   *
   * @param <String>
   *
   */
  d: function debug() {
    wu.debug++;
    wu.l();
    console.log('<==========>');
    console.log('[Debug point ' + wu.debug + ']');
    wu.c(arguments[0]);
    console.log('<==========>');
    wu.l();
    return wu;
  },

  /**
   * l() logs a line, to make a spacing amongst logs.
   *
   * @param <Undefined>
   *
   */
  l: function log() {
    console.log('');
    return wu;
  },

  /**
   * a() asserts if a condition is true and logs a
   * message, stating if it the condition passed.
   * if no message is passed, a general message is
   * logged.
   *
   * @param <Condition> condition
   * @param <String> message
   *
   */
  a: function assert(condition, message) {
    if (!condition) {
      message = message || "Assertion failed";
      console.log("FAIL", message);
      return wu;

    }
    else {
      message = message || "Assertion passed";
      console.log("PASS", message);
      return wu;
    }
  }

};
module.exports = wu;