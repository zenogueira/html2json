var options = {


    data : {
        howFar: 0, // optional, defaults to 1; Tells the script where the table stops reading for value 'cells'.
        // any value bigger than 1 produces an array as a value of that key
        blankProtection: false, // will keep going on the orientation direction if a blank row or column is found in the middle of the table.
        // Will try for 5 times, and then go to next file if cant find another on value. TODO melhorar o nome desta prop
        testFor: 10, // Will scrape and write only the x first results, to test the options defined and the results obtained



        selector: '"tr:nth-child(" + k + ") > td:nth-child(" + v +")"',
        // You're responsible for correctly identifying where are the keys and the values to scrape from.
        k: 2, // key value location
        v: 1,  // value value location //TODO ver melhor como referir isto
        convertDom: { from: '', to: ''}, // Using IConv node module.
        encoding: 'utf8' //defaults to utf8, unless convertDom is used, in which case gets set to null and only convertDom is used.

        // TODO possibilidade de limitar os dados que se sacam, isto é, dimensoes do que se quer scrappear (x e y)
        // TODO possibilidade de não aceitar certas keys para o doc final.
    },
    transformKey: function () {}, // options, transforms the key value between scrapping, and writing to the object with the scrapped data.
    //  By default, for the key we strip the spaces, and other accented characters, and we lower the case.
    transformValue: function () {}, // optional, produces any transformation on the value, before writing it to the object with
    // the scrapped data. By degfault the value is trimmed.
    transformBeforeWrite: function () {}, // optional, produces transformation on the text
    // options.key and options.value are the values on each iteration that will be available for the above functions, even if you .


    id: '_id', // Defaults to id, can be a string with a name (for sequential creation), or a function to dinamically generate one?.
    childOf: 'key', // Parent property on the final json object
    addToObject: {}, // adds this object's keys to the created object. Can be a function, as long as it returns an object.
    inOneJSON: { breakAt: 50 }, // Defaults to true. Outputs the resulting document of all scrapped documents into one JSON file
    // unless { breakAt: Number } is supplied for number of documents per .json file.
    JSONModel: 'object',  // defaults to array: [ {id:'',...}, {id:'',...} ] . in object { id1: {}, id2:{} }
    allowDotfiles: false, // defaults to false TODO later on? não sei se tem interesse
    files: '/data/files',
    keys : {
        'ò' : 'o' // keys to change. Todo utilizar node-parametrize. aí acaba-se a bricnadeira com as keys?
    },
    JSONToWrite: 'scrapped' // Name of the file to write to. Defaults to final.json. Can be on a folder if written in 'folder/file' structure. It shouldn't have the extension.
};

/*
TODO accept a function for id option key that allows for dynamic key name generation
TODO validação dos parametros da seguinte forma: ou estão bem escritos, ou aplica-se o valor default. Se não há um valor default, sacar error
TODO this.testSelector - a ver se foi escolhido bem.
TODO possibilidade de fazer novo scrapping, utilizando so o metodo download()
TODO renomear os metodos..
TODO utilizar módulo de node para parsing da key: https://github.com/fyalavuz/node-parameterize
TODO opcao para transformar um campo em multivalued
TODO validar todos os inputs
TODO acertar o encoding ao escrever o ficheiro final
TODO criar a possibilidade de fazer o download dos links, seguido de parsing
TODO possibilidade de passar o objecto de configuração através de um ficheiro JSON.
TODO criar um mecanismo de numeração do id que aceite funções, que permita a criação de ids customizados
TODO possibilidade de limitar os dados que se sacam, isto é, dimensoes do que se quer scrappear (x e y)
TODO possibilidade de não aceitar certas keys para o doc final.


 */
