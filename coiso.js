/**
 * Created by josenogueira on 21/10/14.
 */
'use strict';

var H2J = require('./html2json_.js');

var c = require('./writeUtils').c;

var options = {
    data : {
        selector: '"tr:nth-child(" + k + ") > td:nth-child(" + v + ")"',
        k: 2,
        v: 1,
        blankProtection: 5,
        convertDom: { from:'ISO-8859-1', to:'UTF-8'}
    },
    //JSONModel: 'array',
    keys: { 'ò': 'o', 'n': 'N'},
    transformKey: function (text) {
        return text
            .slice(0, -1)
            .trim()
            .toLowerCase()
            .replace(/\s/g, '-')
            .replace("º","-","ª", "")
            .replace("--","-","---","-")
            .replace(/[^A-Za-z0-9\[\] ]/g, function (a) {
            return options.keys[a] || a
        });

    },
    transformValue: function (value) {
        var t,obj;
        t = value.trim().split('\n');
        // TODO colocar no obj as keys que se dividem em array e fazer if (obj[options.key] && t.length > 1) { return t } else { return value.trim(); }
        // TODO é melhor assim porque utiliza o sistema de whitelist

        //obj = {
        //    descritores: true
        //    // e por aí fora
        //};

        // Se for caso de separar em array e o array tiver mais que um de length, devolver o array.
        if (options.key !== 'sumario' && options.key !== 'decisao-texto-integral') {
            if (t.length > 1) {
                return t;
            }
        }
        return value.trim();
    },
    inOneJSON: { breakAt: 1},
    files: './downloads'
};


var h2j = new H2J(options);

h2j.start();