'use strict';
// Node Core Modules
var fs = require('fs');

// NPM Modules
var cheerio = require('cheerio');
var async = require('async');

// Other requires
var wu = require('./writeUtils');

// Variables
var c = wu.c;
var d = wu.d;
var $;

// HELPER FUNCTION FOR INPUT VALIDATION
function toType(obj) {
  return ({}).toString.call(obj).match(/\s([a-zA-Z]+)/)[1].toLowerCase()
} // thanks to Angus Croll @ http://javascriptweblog.wordpress.com/2011/08/08/fixing-the-javascript-typeof-operator/


function h2j(options) {
  var _this = this;
  //var options;
  var data;

  // Validation against options.data as it's the most
  // important data, and assumes options is an object
  // as well.
  if (toType(options.data) !== 'object') {
    throw '==> Error: Options object provided isn\'t of type Object.';
  }
  options.keys = options.keys || {};

  if (options.keys && toType(options.keys) !== 'object') {
    throw '==> Error: To add new text replacement keys, ' +
    'they must be a JSON formated object, e.g. keys : { "a" : "b" }';
  }
  //options = options;
  data = options.data;

  options.keys.default = {
    'ã': 'a',
    'á': 'a',
    'à': 'a',
    'â': 'a',
    'ç': 'c',
    'é': 'e',
    'ê': 'e',
    'ó': 'o',
    'õ': 'o',
    'ô': 'o'

  };

  // CHECK IF THERE ARE CUSTOM KEY REPLACEMENTS FOR THE JSON KEYS, AND ADD THE DEFAULT ONES
  if (Object.keys(options.keys).length) {
    for (var prop in options.keys.default) {
      options.keys[prop] = options.keys.default[prop];
    }
  }
  else {
    options.keys = options.keys.default;
  }

  if (!options.transformKey) {

    // By default, for the key we strip the spaces, and other accented characters, and we lower the case.
    options.transformKey = function (text) {
      return text
        .trim()
        .toLowerCase()
        .replace(/\s/g, '')
        .replace(/[^A-Za-z0-9\[\] ]/g, function (a) {
          return options.keys[a] || a
        });
    };
  }

  if (toType(options.transformValue) !== 'function') {
    options.transformValue = function (value) {
      return value.trim();
    }
  }

  if (toType(options.transformBeforeWrite) !== 'function') {
    options.transformBeforeWrite = function (JFinal) {
      return JFinal;
    }
  }

  options.JSONModel = options.JSONModel === 'object' ? 'object' : 'array';

  this.JSONFinal = options.JSONModel === 'object' ? {} : [];

  // VALIDATE USER SUPPLIED OBJECT AND START CONVERTING TO JSON
  this.start = function () {

    // CHECK IF THE SELECTOR IS A STRING WITH THE RIGHT FORMAT (must have the k and v variables!).
    if ((!data.k || !data.k) || (data.selector.replace(/\s/g, '').indexOf('+k+') === -1 || data.selector.replace(/\s/g, '').indexOf('+v+') === -1)) {
      throw '==> Error: CSS selector property value must be of type String, and have the var k and v.'
    }

    // CHECK FOR files PARAM. IF THERE IS NONE, DEFINE PARAM WITH DEFAULT VALUE.
    if (!options.files) {
      options.files = './files'
    }
    else if (toType(options.files) !== 'string') {
      throw '==> Error: Files directory parameter must be of type String.'
    }
    // E links, em vez de ficheiros? TODO Meter links a funcionar também!

    // CHECK FOR ENCODING AND CONVERSION SETTINGS. Defaults to 'utf8'.
    if (!data.encoding) {
      data.encoding = 'utf8'
    }

    if (toType(data.convertDom.from) === 'string' && toType(data.convertDom.to) === 'string') {
      this.Iconv = require('iconv').Iconv;
      data.encoding = null;
    }

    else if (toType(data.encoding !== 'string')) {
      throw '==> Error: Encoding parameter must be of type String. Defaults to "utf8".'
    }
    if (toType(options.inOneJSON.breakAt) === 'number') {
      this.breaks = 1;
      data.testFor = Infinity;
    }

    //// CHECK FOR allowDuplicates PARAM. IF THERE IS NONE, DEFINE PARAM WITH DEFAULT VALUE.
    //if (!options.allowDuplicates) {
    //    options.allowDuplicates = false;
    //}
    //else if (toType(options.allowDuplicates) !== 'boolean') {
    //    throw '==> Error: allowDuplicates parameter must be of type "boolean".'
    //}

    this.download();
  };
  this.download = function () { // TODO repensar o nome deste método!!
    var _files = [], iconv;
    options.currentId = 0;


    // Reads files dir files and starts reading them.
    fs.readdir(options.files, function (err, files) {
      if (err && err.errno == 34) {
        throw '==> Error: No such directory: ' + options.files + '. Maybe it needs to be created?';
      }
      else if (err && err.errno != 34) {
        console.log(err.errno);
      }

      if (files == '') {
        throw '==> Error: No files found on files dir. Aborting.';
      }


      var processFiles = function (file, callback) {
        if (file.charAt(0) != '.') {
          var fileName = options.files + '/' + file;
          console.log('just opened file' + fileName);
          fs.exists(fileName, function (exists) {
            if (exists) {
              fs.stat(fileName, function (error, stats) {
                fs.open(fileName, "r", function (error, fd) {

                  var buffer = new Buffer(stats.size);
                  fs.read(fd, buffer, 0, buffer.length, null, function (err, bytesRead, buffer) {
                    var dom = buffer.toString(data.encoding, 0, buffer.length);

                    if (err) {
                      console.log('==> Error: Could not read file ');
                    }

                    if (toType(data.convertDom.from) == 'string' && toType(data.convertDom.to) == 'string') {
                      iconv = new _this.Iconv(data.convertDom.from, data.convertDom.to);
                      dom = iconv.convert(dom).toString();
                    } else {
                      dom = data.toString();
                    }
                    console.log('just read a file ' + fileName);
                    fs.close(fd);
                    _this.JSON(dom); // Transforms the html into a JSON object
                    callback();
                  });
                });
              });
            }
          });
        }
      };

      async.eachLimit(files, 2, processFiles, function (err) {
        if (err) {
          console.log(err);
        }
        console.log('terminado');
      });

    });

  };
  this.JSON = function (dom) {

    $ = cheerio.load(dom);

    var k, v, key, val, hF,
      doc = {},
      bp = 1;

    k = data.k;
    v = data.v;
    hF = toType(data.howFar) == 'number' ? data.howFar : 1;

    // Start iteration of the DOM
    var doingDOM = function (callback) {
      key = eval(data.selector);

      if ($(key).text()) {
        bp = 1; // Resets bp when going for a new row or column

        // Apply transformKey function.
        options.key = options.transformKey($(key).text());

        // set the value for the key value, with due transformations if necessary, and then loop and arrayify if hF > 1.
        v++;
        val = eval(data.selector);
        options.value = options.transformValue($(val).text());
        var _wHFi = 0;
        (function whileHF() {
          _wHFi++;
          if (hF > 1 && _wHFi <= hF) {
            if (toType(options.value) !== 'array') {
              options.value = [options.value];
            }
            v++;
            val = eval(data.selector);
            options.value.push(options.transformValue($(val).text()));
            return whileHF;
          }


          //Continuing doingDOM:

          // Put increased value back to its value, so on the next cycle can start on
          // the property key and then move to read the value key(s).
          v = data.v;

          // setting the keys
          doc[options.key] = options.value;
          k++;
          setTimeout(callback, 0);

        })();


      }
      else { // end of table, or middle empty row or column. Treat accordingly, and then proceed to add to the object.


        if (toType(data.blankProtection) === 'number') {
          if (bp == data.blankProtection) {
            k = 0;
            v = 0;
            setTimeout(callback, 0);

          }
          else {
            bp++;
            k++;
            setTimeout(callback, 0);

          }


        }
        else {
          k = 0;
          v = 0;
          setTimeout(callback, 0);

        }

      }
    };

    var afterEachDOM = function afterEachDOM() {

      var toWrite = function (doc) {
        var fn = toType(data.JSONToWrite) === 'string' ? data.JSONToWrite : 'final';
        console.log("Writing " + fn);


        if (toType(options.inOneJSON.breakAt) === 'number') {
          if (_this.JSONFinal.length < options.inOneJSON.breakAt) {
            if (options.JSONModel === 'object') {
              if (toType(options.id) === 'string') {
                _this.JSONFinal[options.id + options.currentId] = doc; //  CUSTOM ID FIELD
                // TODO criar um mecanismo de numeração do id que aceite funções, que permita a criação de ids customizados
              } else {
                _this.JSONFinal['id' + options.currentId] = doc;
              }
            } else {
              _this.JSONFinal.push(doc);
            }
          }

          if (_this.JSONFinal.length === options.inOneJSON.breakAt) {
            var jsonToWrite = _this.JSONFinal;
            _this.JSONFinal = options.JSONModel == 'object' ? {} : [];
            _this.write(fn + _this.breaks++, jsonToWrite);
          }
        }
        else {
          // TODO: review this case no breakAt defined -> just one write
          _this.JSONFinal.push(doc);


          // TODO corrigir para producao
          _this.write(fn + _this.breaks++, function () {
            _this.JSONFinal = options.JSONModel == 'object' ? {} : [];
          });
        }
      };

      options.currentId++; // increase sequential id value for every scanned doc.
      // TODO criar um mecanismo de numeração do id que aceite funções, que permita a criação de ids customizados

      // If JSONModel is an array, we're going to add the id inside the doc object and push it to the JSONFinal,
      // else add the id property  as a key into the JSONFinal object and the doc object as its value.
      if (options.JSONModel == 'array') { // Comparing against the JSONFinal property because it's already validated, while JSONModel is not.
        if (toType(options.id) === 'string') { //  CUSTOM ID FIELD
          doc[options.id] = options.currentId;
        } else {
          doc.id = options.currentId;
        }
      }
      toWrite(doc);
    };

    async.whilst(function () {
      return (k && v);
    }, doingDOM, afterEachDOM);


  };
  this.write = function (fn, jsonToWrite) {

    fs.writeFile('final/' + fn + '.json', options.transformBeforeWrite(JSON.stringify(jsonToWrite)), function (err) {
      if (err) {
        console.log('Error writing JSON file: ', err);
      }
      else {
        console.log('Sucessfully written file ' + fn + '.json');
      }
    });


  };
  this.testSelector = {}; // para testar o selector e fazer print das primeiras dez linhas;
}


module.exports = h2j;
